 Lab 3

## Configure python virtual environment 

https://realpython.com/python-virtual-environments-a-primer/

## Django tutorials

https://docs.djangoproject.com/en/3.1/intro/tutorial01/

For details see: [Official Python tutorial](https://docs.python.org/3/tutorial/index.html)


#Django installation

 1  git clone https://gitlab.com/mantolovic/se_labs.git
    2  cd se_labs
    3  ls
    4  git status
    5  git remote add upstream https://gitlab.com/levara/se_labs
    6  git fetch upstream master
    7  git merge upstream/master
    8  git config user.email "antolovicmihael@gmail.com
    9  git config user.email "antolovicmihael@gmail.com"
   10  git config user.name "mantolovic"
   11  git merge upstream/master
   12  git status
   13  git push origin master
   14  cd lab3
   15  ls
   16  mkdir venv
   17  cd venv
   18  python -m venv env
   19  ls
   20   which python3
   21  which python
   22  ls env/
   23  source env/Scripts/activate
   24  which python
   25  pip install django
   26  history