# Lab 2

## First solve these exercises

1. Napiši program koji će generirati slučajni broj između 0 i 15, te će tražiti od korisnika da pogodi koji je to broj. Kada pogodi, ispisati broj pokušaja.
2. Napiši program koji će tražiti od korisnika unos imena ulica, gdje imena ulica smiju biti između 7 i 15 znakova. Unositi imena dok se ne unese riječ prekid, nakon čega program treba ispisati koliko je imena ulica unešeno, te koje ime ulice je najduže.


## Contents

- Dictionaries and tuples
- List comprehension
- Classes
- File IO


For details see: [Official Python tutorial](https://docs.python.org/3/tutorial/index.html)


