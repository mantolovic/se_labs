def up_low_num(str):
    upper_count=0
    lower_count=0
    num_count=0
    for x in str:
        if x.isupper():
            upper_count+=1
        elif x.islower():
            lower_count+=1
        elif x.isnumeric():
            num_count+=1

    print(f'number of upper case letter {upper_count}')
    print(f'number of lower case letter {lower_count}')
    print(f'number of numbers {num_count}')

def main():
    checkString="asdf98CXX21grrr"
    print(f"{up_low_num(checkString)}")

if __name__ == "__main__":
    main()