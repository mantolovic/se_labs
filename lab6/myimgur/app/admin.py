from django.contrib import admin
from .models import Image, Comment

class CommentInline(admin.StackedInline):
    model = Comment
    extra = 0

class ImageAdmin(admin.ModelAdmin):
    inlines=[CommentInline]
# Register your models here.

admin.site.register(Image, ImageAdmin)